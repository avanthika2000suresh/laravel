
{{-- list of all list page --}}
<h1>{{$heading}}</h1>
@foreach($listings as $listing)
    <h2>
        {{-- making links for the title to go to the single list page --}}
       <a href="/listings/{{$listing['id']}}">
        {{$listing['title']}}
    </a>
    </h2>
    <p>
        {{$listing['description']}}
    </p>
 @endforeach  